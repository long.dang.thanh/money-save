package bpc.money.save.be.web.api;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import bpc.money.save.be.orm.model.Product;
import bpc.money.save.be.web.AbstractController;

@RestController
public class ProductDemoController extends AbstractController {
	@GetMapping("/add-session2")
public String add(HttpSession session) {
		Product product = new Product(2, "knife", (double) 40000, "sharp");
		session.setAttribute("product", product);
		System.out.println("add session");
		return "added2";
	}
	@GetMapping("/product")
	public Product get(HttpSession session) {
		Product product = (Product) session.getAttribute("product");
		return product;
	}
	

}
