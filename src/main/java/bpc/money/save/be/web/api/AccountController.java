package bpc.money.save.be.web.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bpc.money.save.be.web.AbstractController;

@RestController
public class AccountController extends AbstractController {

	@GetMapping("/account")
	public Account get(@RequestParam(name = "name", required = false) String name) {
		//check login or not
		Account account = new Account(name, "16");
		return account;
	}
	
	@PostMapping("/account")
	public Account post(@RequestParam(name = "name", required = false) String name, @RequestParam(name="age", required = false) String age) {
		Account account = new Account(name, age);
		return account;
	}
	
	class Account {
		private String name;
		private String age;
		
		public Account(String name, String age) {
			super();
			this.name = name;
			this.age = age;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getAge() {
			return age;
		}

		public void setAge(String age) {
			this.age = age;
		}
		
	}
}
