package bpc.money.save.be.web.api;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import bpc.money.save.be.orm.model.Account;
import bpc.money.save.be.web.AbstractController;

@RestController
public class SessionDemoController extends AbstractController {
	@GetMapping("/add-session")
	public String add(HttpSession session) {
		Account account = new Account(1, "aa", 18, "Hanoi");
		session.setAttribute("account", account);
		System.out.println("add session");
		return "added";
	}
	
	@GetMapping("/get-session")
	public Account get(HttpSession session) {
		Account account = (Account) session.getAttribute("account");
		return account;
	}
}
