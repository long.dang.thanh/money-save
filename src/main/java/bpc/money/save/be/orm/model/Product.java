package bpc.money.save.be.orm.model;

public class Product {
	private Integer id;
	private String name;
	private Double price;
	private String note;
	public Product(Integer id, String name, Double price, String note) {
		super();
		this.id = id;
		this.name = name;
		this.price = price;
		this.note = note;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	

}
